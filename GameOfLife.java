public class GameOfLife {

    private int rowCount;
    private int columnCount;

    public int[][] grid;

    public GameOfLife(int nbRow, int nbColumn) {
        grid = new int [nbRow][nbColumn];

        // TODO: 10/01/2021 pourquoi recalculer les tailles du tableau
        this.rowCount = grid.length;
        this.columnCount = grid[0].length;

        // TODO: 10/01/2021 qu'est ce que tu fais ici ??
        for (int y = 0; y < this.rowCount; y++) {
            for (int x = 0; x < this.rowCount; x++) {
                grid[y][x] = 0;
            }
        }
    }

    // TODO: 10/01/2021 revoir le nommage, c'est comme si la méthode s'appelait doSomething()
    // TODO: 10/01/2021 n'hésite pas à utiliser du langage métier dans ton code faciliter la compréhension
    public void generateNextState() {

        int[][] nextState = new int[rowCount][columnCount];

        for (int y = 0; y < rowCount; y++) {

            // TODO: 10/01/2021 fonctionnement trop complexe, aide toi de méthodes privées
            for (int x = 0; x < columnCount; x++) {

                // TODO: 10/01/2021 c'est top la représentation :)
                int[][] toBeChecked = {
                        {y - 1, x - 1},     {y - 1, x},         {y - 1, x + 1},
                        {y, x + 1},         /* cell courante*/  {y + 1, x + 1},
                        {y + 1, x},         {y + 1, x - 1},     {y, x - 1},
                };

                // TODO: 10/01/2021 Qu'est ce que ça compte ??
                int cpt = 0;

                int nbCellToCheck = 8;
                for (int i = 0; i < nbCellToCheck; i++) {
                    int currRow = toBeChecked[i][0];
                    int currCol = toBeChecked[i][1];

                    // TODO: 10/01/2021 Si tu as besoin d'un commentaire c'est que ça mérite un petit refactoring
                    // vérifie si on est dans les limites de la grille et que la cellule est en vie
                    if (currRow >= 0 && currCol >= 0 && currRow < rowCount && currCol < columnCount && grid[currRow][currCol] == 1) {
                        cpt ++ ;
                    }

                }

                // TODO: 10/01/2021 De même ici n'hésite pas à t'aider de méthodes privées pour la clarté
                // passe en vie si 3 voisins sont en vie
                if (cpt == 3 && grid[y][x] == 0) {
                    nextState[y][x] = 1;

                // meurt si moins de 2 voisins en vie
                } else if (cpt < 2 && grid[y][x] == 1) {
                    nextState[y][x] = 0;

                // reste en vie si 2 ou 3 voisins en vie
                } else if ((cpt == 2 || cpt == 3) && grid[y][x] == 1) {
                    nextState[y][x] = 1;

                // meurt si plus de 3 voisins en vie
                } else if (cpt > 3 && grid[y][x] == 1) {
                    nextState[y][x] = 0;
                // cas général
                } else {
                    nextState[y][x] = grid[y][x];
                }
            }
        }
        grid = nextState.clone();
    }

    // TODO: 10/01/2021 pourquoi 1 ?? pourquoi pas 299381013 ?? + problème de nommage
    public void getAlive (int row, int column) {
        grid[row][column] = 1;
    }
}