/***
 * Examen module Java : Le jeu de la vie
 * Rappel (Wikipedia) :
 * Le jeu de la vie est un « jeu à zéro joueur », puisqu'il ne nécessite pas l'intervention du joueur lors de son
 * déroulement. Il s’agit d’un automate cellulaire, un modèle où chaque état conduit mécaniquement à l’état suivant à
 * partir de règles pré-établies.
 *
 * Le jeu se déroule sur une grille à deux dimensions, théoriquement infinie (mais de longueur et de largeur finies et
 * plus ou moins grandes dans la pratique), dont les cases — qu’on appelle des « cellules », par analogie avec les
 * cellules vivantes — peuvent prendre deux états distincts : « vivante » ou « morte ».
 *
 * Une cellule possède huit voisins, qui sont les cellules adjacentes horizontalement, verticalement et diagonalement.
 *
 * À chaque étape, l’évolution d’une cellule est entièrement déterminée par l’état de ses huit voisines de la
 * façon suivante :
 **
 * une cellule morte possédant exactement trois voisines vivantes devient vivante (elle naît) ;
 * une cellule vivante possédant deux ou trois voisines vivantes le reste, sinon elle meurt.
 *
 *
 * Objectif (2h) : implémenter une classe GameOfLife capable de simuler le jeu dans le terminal
 *
 * Pour valider l'implémentation, faites tourner le jeu sur 18 générations avec cette grille (10x10) de départ :
 *          ..........
 *          ..........
 *          ..........
 *          ..........
 *          .....*....
 *          ...*.*....
 *          ....**....
 *          ..........
 *          ..........
 *          ..........
 *
 *
 * Aide pour l'affichage :
 * Effacer terminal :
 *      System.out.print("\033[H\033[2J");
 *      System.out.flush();
 *
 *
 * Mettre en pause l'affichage :
 *      Thread.sleep(x);
 * avec x en millisecondes
 */

public class Application {
    public static void main(String[] args) throws InterruptedException {
        GameOfLife game = new GameOfLife(10,10);
        game.getAlive(4,5);
        game.getAlive(5,3);
        game.getAlive(5,5);
        game.getAlive(6,4);
        game.getAlive(6,5);

        for (int i = 0 ; i < 18 ; i++) {
            System.out.print("\033[H\033[2J");
            System.out.flush();
            printGrid(game);
            // TODO: 10/01/2021 Il faut gérer l'éventuelle exception
            Thread.sleep(500);
            game.generateNextState();
        }
    }


    // TODO: 10/01/2021 Pourquoi cette méthode est ici ?? elle est quand même très liée à la classe GameOfLife ??
    // TODO: 10/01/2021 Pourquoi être passé par des 0 et des 1 si tu veux afficher des . et des * ??
    public static void printGrid(GameOfLife game) {
        int x_size = game.grid.length;
        int y_size = game.grid[0].length;

        for (int x = 0; x < x_size ; x++) {
            for (int y = 0; y < y_size ; y++) {
                if (game.grid[x][y] == 0) System.out.print(".");
                else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        System.out.println();

    }
}
